#!/bin/bash

if [[ "$(whoami)" != root ]]; then
	echo "${kirmizi}İlk Önce Root Olmanız Gerekmekte.."
	exit 0
fi

u="$SUDO_USER"
rm -rf /opt/pckapat/
rm /usr/share/polkit-1/actions/org.freedesktop.policykit.pkexec.pckapat.policy 
rm /home/$u/Masaüstü/PcKapat.desktop 
rm /usr/share/applications/PcKapat.desktop
