# PcKapat

Ayarlan süre sonunda seçilen görevi yapan program.

Qt5 ve C++ kullanılarak yazılmıştır.

Kurulum için root yetkisi ile terminalden;

./kur.sh

yazın.

Programı kaldırmak için root yetkisi ile terminalden;

./kurulumusil.sh

yazın.

Ekran görüntüsü:

<img src="PcKapatEkran.png" width="auto">
