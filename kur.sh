#!/bin/bash

if [[ "$(whoami)" != root ]]; then
	echo "${kirmizi}İlk Önce Root Olmanız Gerekmekte.."
	exit 0
fi

u="$SUDO_USER"
mkdir /opt/pckapat
cp -r * /opt/pckapat/
cp org.freedesktop.policykit.pkexec.pckapat.policy /usr/share/polkit-1/actions/
cp ./PcKapat.desktop /home/$u/Masaüstü/
cp ./PcKapat.desktop /usr/share/applications/
