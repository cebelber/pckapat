/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.11
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionProgram_Hakk_nda;
    QAction *actionQt_Hakk_nda;
    QAction *action_k;
    QWidget *centralwidget;
    QGroupBox *groupBox;
    QLabel *label_3;
    QSpinBox *saatSpinBox;
    QSpinBox *dakikaSpinBox;
    QLabel *label;
    QSpinBox *saniyeSpinBox;
    QLabel *label_2;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout;
    QRadioButton *kapatRadioButton;
    QRadioButton *yenidenRadioButton;
    QRadioButton *oturumRadioButton;
    QRadioButton *komutRadioButton;
    QLCDNumber *lcdSaat;
    QLCDNumber *lcdKalanSure;
    QLabel *label_7;
    QLabel *label_8;
    QPushButton *baslaButton;
    QPushButton *exitButton;
    QPushButton *iptalButton;
    QLineEdit *komutLineEdit;
    QLabel *label_4;
    QMenuBar *menubar;
    QMenu *menuHakk_nda;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(380, 380);
        MainWindow->setMinimumSize(QSize(380, 380));
        MainWindow->setMaximumSize(QSize(380, 380));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/simge/simgeler/saat.ico"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setWindowOpacity(14.000000000000000);
        actionProgram_Hakk_nda = new QAction(MainWindow);
        actionProgram_Hakk_nda->setObjectName(QString::fromUtf8("actionProgram_Hakk_nda"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/simge/simgeler/info.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionProgram_Hakk_nda->setIcon(icon1);
        actionQt_Hakk_nda = new QAction(MainWindow);
        actionQt_Hakk_nda->setObjectName(QString::fromUtf8("actionQt_Hakk_nda"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/simge/simgeler/Qt.ico"), QSize(), QIcon::Normal, QIcon::Off);
        actionQt_Hakk_nda->setIcon(icon2);
        action_k = new QAction(MainWindow);
        action_k->setObjectName(QString::fromUtf8("action_k"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/simge/simgeler/exit.png"), QSize(), QIcon::Normal, QIcon::Off);
        action_k->setIcon(icon3);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(180, 60, 181, 81));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(126, 30, 41, 21));
        saatSpinBox = new QSpinBox(groupBox);
        saatSpinBox->setObjectName(QString::fromUtf8("saatSpinBox"));
        saatSpinBox->setGeometry(QRect(16, 50, 43, 24));
        saatSpinBox->setMaximum(24);
        dakikaSpinBox = new QSpinBox(groupBox);
        dakikaSpinBox->setObjectName(QString::fromUtf8("dakikaSpinBox"));
        dakikaSpinBox->setGeometry(QRect(70, 50, 43, 24));
        dakikaSpinBox->setMaximum(59);
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(22, 30, 31, 21));
        saniyeSpinBox = new QSpinBox(groupBox);
        saniyeSpinBox->setObjectName(QString::fromUtf8("saniyeSpinBox"));
        saniyeSpinBox->setGeometry(QRect(126, 50, 43, 24));
        saniyeSpinBox->setMaximum(59);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(69, 30, 41, 21));
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(10, 60, 150, 198));
        verticalLayout = new QVBoxLayout(groupBox_2);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        kapatRadioButton = new QRadioButton(groupBox_2);
        kapatRadioButton->setObjectName(QString::fromUtf8("kapatRadioButton"));

        verticalLayout->addWidget(kapatRadioButton);

        yenidenRadioButton = new QRadioButton(groupBox_2);
        yenidenRadioButton->setObjectName(QString::fromUtf8("yenidenRadioButton"));

        verticalLayout->addWidget(yenidenRadioButton);

        oturumRadioButton = new QRadioButton(groupBox_2);
        oturumRadioButton->setObjectName(QString::fromUtf8("oturumRadioButton"));

        verticalLayout->addWidget(oturumRadioButton);

        komutRadioButton = new QRadioButton(groupBox_2);
        komutRadioButton->setObjectName(QString::fromUtf8("komutRadioButton"));

        verticalLayout->addWidget(komutRadioButton);

        lcdSaat = new QLCDNumber(centralwidget);
        lcdSaat->setObjectName(QString::fromUtf8("lcdSaat"));
        lcdSaat->setGeometry(QRect(60, 6, 231, 51));
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Sans"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        lcdSaat->setFont(font);
        lcdSaat->setStyleSheet(QString::fromUtf8("background-color: rgb(90, 164, 186);\n"
"color: rgb(255, 255, 255);"));
        lcdSaat->setDigitCount(8);
        lcdKalanSure = new QLCDNumber(centralwidget);
        lcdKalanSure->setObjectName(QString::fromUtf8("lcdKalanSure"));
        lcdKalanSure->setGeometry(QRect(180, 168, 181, 51));
        lcdKalanSure->setStyleSheet(QString::fromUtf8("background-color: rgb(248, 87, 54);\n"
"color: rgb(255, 255, 255);"));
        lcdKalanSure->setDigitCount(8);
        label_7 = new QLabel(centralwidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(10, 20, 41, 16));
        label_8 = new QLabel(centralwidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(180, 148, 81, 16));
        baslaButton = new QPushButton(centralwidget);
        baslaButton->setObjectName(QString::fromUtf8("baslaButton"));
        baslaButton->setGeometry(QRect(20, 305, 91, 23));
        exitButton = new QPushButton(centralwidget);
        exitButton->setObjectName(QString::fromUtf8("exitButton"));
        exitButton->setGeometry(QRect(270, 305, 80, 23));
        iptalButton = new QPushButton(centralwidget);
        iptalButton->setObjectName(QString::fromUtf8("iptalButton"));
        iptalButton->setGeometry(QRect(120, 305, 80, 23));
        komutLineEdit = new QLineEdit(centralwidget);
        komutLineEdit->setObjectName(QString::fromUtf8("komutLineEdit"));
        komutLineEdit->setGeometry(QRect(70, 268, 281, 23));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(15, 270, 57, 15));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 380, 20));
        menuHakk_nda = new QMenu(menubar);
        menuHakk_nda->setObjectName(QString::fromUtf8("menuHakk_nda"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);
        QWidget::setTabOrder(saatSpinBox, dakikaSpinBox);
        QWidget::setTabOrder(dakikaSpinBox, saniyeSpinBox);
        QWidget::setTabOrder(saniyeSpinBox, kapatRadioButton);
        QWidget::setTabOrder(kapatRadioButton, yenidenRadioButton);
        QWidget::setTabOrder(yenidenRadioButton, oturumRadioButton);
        QWidget::setTabOrder(oturumRadioButton, baslaButton);
        QWidget::setTabOrder(baslaButton, exitButton);

        menubar->addAction(menuHakk_nda->menuAction());
        menuHakk_nda->addAction(actionProgram_Hakk_nda);
        menuHakk_nda->addAction(actionQt_Hakk_nda);
        menuHakk_nda->addSeparator();
        menuHakk_nda->addAction(action_k);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "PC Kapat", nullptr));
        actionProgram_Hakk_nda->setText(QApplication::translate("MainWindow", "Program Hakk\304\261nda", nullptr));
        actionQt_Hakk_nda->setText(QApplication::translate("MainWindow", "Qt Hakk\304\261nda", nullptr));
        action_k->setText(QApplication::translate("MainWindow", "\303\207\304\261k\304\261\305\237", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "S\303\274re", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Saniye", nullptr));
        label->setText(QApplication::translate("MainWindow", "Saat", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Dakika", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "\304\260\305\237lem Se\303\247in", nullptr));
        kapatRadioButton->setText(QApplication::translate("MainWindow", "Bilgisayar\304\261 Kapat", nullptr));
        yenidenRadioButton->setText(QApplication::translate("MainWindow", "Yeniden Ba\305\237lat", nullptr));
        oturumRadioButton->setText(QApplication::translate("MainWindow", "Oturumu Kapat", nullptr));
        komutRadioButton->setText(QApplication::translate("MainWindow", "Komut \303\207al\304\261\305\237t\304\261r", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "Saat ", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Kalan S\303\274re", nullptr));
        baslaButton->setText(QApplication::translate("MainWindow", "\304\260\305\237lemi Ba\305\237lat", nullptr));
        exitButton->setText(QApplication::translate("MainWindow", "\303\207\304\261k\304\261\305\237", nullptr));
        iptalButton->setText(QApplication::translate("MainWindow", "\304\260ptal", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "Komut :", nullptr));
        menuHakk_nda->setTitle(QApplication::translate("MainWindow", "Men\303\274", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
