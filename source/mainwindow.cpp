/*****************************************************************************
 *   This program is coded by Celalettin AKARSU                              *
 *   Copyright (C) 2022                                                      *
 *   <akarsu@protonmail.com>                                                 *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   https://www.gnu.org/licenses/licenses.html                              *
 *****************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTime>
#include <QTimer>
#include <QDebug>
#include <QMessageBox>
#include <QProcess>
#include <unistd.h>

#include<QDir>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QTimer *nTimer=new QTimer();
    ui->lcdKalanSure->display("00:00:00");
    connect(nTimer,SIGNAL(timeout()),this,SLOT(SaatGoster()));
    nTimer->start(100);

    connect(sayac,SIGNAL(timeout()),this,SLOT(SayacGoster()));

    user = qgetenv("USER");
    //qDebug()<<"Kullanıcı : "<<user;
}

void MainWindow::SaatGoster(){
    ui->lcdSaat->display(QTime::currentTime().toString());
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_exitButton_clicked()
{
    close();
}


void MainWindow::on_baslaButton_clicked()
{
    if(ui->kapatRadioButton->isChecked() || ui->yenidenRadioButton->isChecked() || ui->oturumRadioButton->isChecked() || ui->komutRadioButton->isChecked())
    {
        ui->baslaButton->setEnabled(false);
        ui->kapatRadioButton->setEnabled(false);
        ui->yenidenRadioButton->setEnabled(false);
        ui->oturumRadioButton->setEnabled(false);
        ui->komutRadioButton->setEnabled(false);
        ui->exitButton->setEnabled(false);

        saat = ui->saatSpinBox->value();
        dk = ui->dakikaSpinBox->value();
        sn = ui->saniyeSpinBox->value();

        zsaat = QString("%1").arg(saat, 2, 10, QLatin1Char('0'));
        zdk = QString("%1").arg(dk,2,10,QLatin1Char('0'));
        zsn = QString("%1").arg(sn,2,10,QLatin1Char('0'));
        zaman = zsaat +":"+zdk+":"+zsn;
        ui->lcdKalanSure->display(zaman);


        sayac->start(1000); //1000 olacak
    }
    else{
        QMessageBox mesaj;
        mesaj.setText("Seçim Yapın");
        mesaj.exec();
    }
}

void MainWindow::SayacGoster()
{
    QProcess islem;

    if(sn>0){
         sn--;          
         zsaat = QString("%1").arg(saat, 2, 10, QLatin1Char('0'));
         zdk = QString("%1").arg(dk,2,10,QLatin1Char('0'));
         zsn = QString("%1").arg(sn,2,10,QLatin1Char('0'));
         zaman = zsaat +":"+zdk+":"+zsn;

         ui->lcdKalanSure->display(zaman);
     }
     else if ((sn==0) and (dk>0)){
         sn=59;
         dk--;

         zsaat = QString("%1").arg(saat, 2, 10, QLatin1Char('0'));
         zdk = QString("%1").arg(dk,2,10,QLatin1Char('0'));
         zsn = QString("%1").arg(sn,2,10,QLatin1Char('0'));
         zaman = zsaat +":"+zdk+":"+zsn;

         ui->lcdKalanSure->display(zaman);
     }
    else if ((sn==0) and (dk==0) and (saat>0)) {
        sn=59;
        dk=59;
        saat--;

        zsaat = QString("%1").arg(saat, 2, 10, QLatin1Char('0'));
        zdk = QString("%1").arg(dk,2,10,QLatin1Char('0'));
        zsn = QString("%1").arg(sn,2,10,QLatin1Char('0'));
        zaman = zsaat +":"+zdk+":"+zsn;

        ui->lcdKalanSure->display(zaman);
    }

     else if ((saat==0) and (dk==0) and (sn==0)) {
         sayac->stop();

         ui->saatSpinBox->setValue(0);
         ui->dakikaSpinBox->setValue(0);
         ui->saniyeSpinBox->setValue(0);

         if(ui->kapatRadioButton->isChecked())
         {
             islem.execute("poweroff");
         }
         else if(ui->yenidenRadioButton->isChecked())
         {
            islem.execute("reboot");
         }

         else if(ui->oturumRadioButton->isChecked())
         {
             islem.execute("pkill -KILL -u "+user);
         }

         else if(ui->komutRadioButton->isChecked())
         {
             islem.execute(ui->komutLineEdit->text());
         }


     }
}

void MainWindow::on_saatSpinBox_valueChanged(int arg1)
{
    if (ui->saatSpinBox->value()==24)
    {
        ui->dakikaSpinBox->setEnabled(false);
        ui->saniyeSpinBox->setEnabled(false);
        ui->dakikaSpinBox->setValue(0);
        ui->saniyeSpinBox->setValue(0);
    }
}


void MainWindow::on_actionProgram_Hakk_nda_triggered()
{
    QString hakk ="PC Kapat\n";
            hakk +="2021\n";
            hakk +="Akarsu\n";
            hakk +="akarsu@protonmail.com";
            QMessageBox::about(this,"..:: Hakkında ::..",hakk);
}


void MainWindow::on_actionQt_Hakk_nda_triggered()
{
    QApplication::aboutQt();
}


void MainWindow::on_action_k_triggered()
{
    QApplication::exit();
}


void MainWindow::on_iptalButton_clicked()
{
    sayac->stop();
    ui->saatSpinBox->setValue(0);
    ui->dakikaSpinBox->setValue(0);
    ui->saniyeSpinBox->setValue(0);

    ui->lcdKalanSure->display("00:00:00");
    ui->baslaButton->setEnabled(true);
    ui->kapatRadioButton->setEnabled(true);
    ui->yenidenRadioButton->setEnabled(true);
    ui->oturumRadioButton->setEnabled(true);
    ui->komutRadioButton->setEnabled(true);
    ui->exitButton->setEnabled(true);
}

