#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QTime>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    int saat,dk,sn;
    QString zaman, zsaat,zdk,zsn;
    QTimer *sayac=new QTimer();
    QByteArray sudoPwd;
    QString user;

private slots:
    void SaatGoster();
    void SayacGoster();
    void on_exitButton_clicked();

    void on_baslaButton_clicked();

    void on_saatSpinBox_valueChanged(int arg1);


    void on_actionProgram_Hakk_nda_triggered();

    void on_actionQt_Hakk_nda_triggered();

    void on_action_k_triggered();

    void on_iptalButton_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
